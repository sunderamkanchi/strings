function string(input) {
  let month = [
    "January",
    "February",
    "March",
    "april",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  let result = input.substr(0, 2);
  let num = Number(result);
  return month[num - 1];
}

module.exports = string;
