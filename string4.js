function string(obj1, obj2) {
  let total = [];
  let str1 = obj1.first_name + " " + obj2.last_name;
  let str2 = obj2.first_name + " " + obj2.middle_name + " " + obj2.last_name;
  let arr = [];
  arr.push(str1, str2);
  for (let i = 0; i < arr.length; i++) {
    let str = arr[i].toLowerCase().split(" ");
    for (let j = 0; j < str.length; j++) {
      str[j] = str[j].charAt(0).toUpperCase() + str[j].slice(1);
    }
    total.push(str.join(" "));
  }
  return total;
}

module.exports = string;
