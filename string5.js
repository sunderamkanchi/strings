function string(input) {
  if (input.length === 0) {
    return " ";
  } else {
    return input.join(" ");
  }
}

module.exports = string;
