function string(input) {
  let str = input.split(".");
  let num = str.map(Number);
  let result = 0;
  for (let i = 0; i < num.length; i++) {
    if (isNaN(num[i])) {
      result += 1;
    }
  }
  if (num.length === 4 && result === 0) {
    return num;
  } else {
    return [];
  }
}

module.exports = string;
