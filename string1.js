function string(input) {
  let total = [];
  let num = [];
  for (let i = 0; i < input.length; i++) {
    let str = input[i].replace(/[$,]/g, "");
    num.push(parseFloat(str));
  }
  for (let i = 0; i < num.length; i++) {
    total.push(isNaN(num[i]) ? 0 : num[i]);
  }
  return total;
}

module.exports = string;
